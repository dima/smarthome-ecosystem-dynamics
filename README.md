# Smarthome Ecosystem Dynamics

Houses the program code and data from research project 2, analyzing membership dynamics between standard-based (platform) ecosystems in the smart home market.